#include once <printnum.asm>
#include once <div16.asm>
#include once <neg16.asm>
#include once <attr.asm>
;
__PRINTI16:	; Prints a 16bits signed in HL
; Converts 16 to 32 bits
                     ;- PROC
                     ;- LOCAL __PRINTU_LOOP
lda z80_h            ;- ld a,h
sta z80_a
ora z80_a            ;- or a
jpl __PRINTU16       ;- jp p, __PRINTU16
jsr __PRINT_MINUS    ;- call __PRINT_MINUS
jsr __NEGHL          ;- call __NEGHL

__PRINTU16:
lda %0               ;- ld b,0
sta z80_b

__PRINTU_LOOP:
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
jeq __PRINTU_START   ;- jp z, __PRINTU_START
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda #<10             ;- ld de,10
sta z80_e
lda #>10
sta z80_d
jsr __DIVU16_FAST    ;- call __DIVU16_FAST ; Divides by DE. DE = MODULUS at exit. Since < 256, E = Modulus
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_e            ;- ld a,e
sta z80_a
ora #$30             ;- or '0'		  ; Stores ASCII digit (must be print in reversed order)
pha                  ;- push af
php
inc z80_b            ;- inc b
jmp __PRINTU_LOOP    ;- jp __PRINTU_LOOP ; Uses JP in loops
                     ;- ENDP

