; vim:ts=4:et:
; FASTCALL bitwise or 16 version.
; result in HL
; __FASTCALL__ version (operands: A, H)
; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL OR DE
;
__BOR16:
lda z80_h      ;- ld a,h
sta z80_a
ora z80_d      ;- or d
lda z80_a      ;- ld h,a
sta z80_h
lda z80_l      ;- ld a,l
sta z80_a
ora z80_e      ;- or e
lda z80_a      ;- ld l,a
sta z80_l
rts            ;- ret

