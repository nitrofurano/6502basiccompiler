; Parameter / Local var load
; A => Offset
; IX = Stack Frame
; RESULT: HL => IX + DE
;
#include once <iloadf.asm>
;
__PLOADF:
lda z80_ix            ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
clc                  ;- add hl,de
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
jmp __LOADF          ;- jp __LOADF

