__PISTORE32:
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_l            ;- add hl,bc
clc
adc z80_c
sta z80_l
lda z80_h
adc z80_b
sta z80_h
pla                  ;- pop bc
sta z80_b
pla
sta z80_c

__ISTORE32:  ; Load address at hl, and stores E,D,B,C integer at that address
ldy #$00             ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_a            ;- ld l,a
sta z80_l
__STORE32:	; Stores the given integer in DEBC at address HL
lda z80_c            ;- ld (hl),c
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_b            ;- ld (hl),b
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_e            ;- ld (hl),e
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_d            ;- ld (hl),d
ldy #$00
sta (z80_hl),y
rts                  ;- ret

