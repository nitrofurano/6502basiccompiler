__SHRA32: ; Right Arithmetical Shift 32 bits
lda z80_d      ;- sra d
cmp #$80
ror @
ror z80_e      ;- rr e
ror z80_h      ;- rr h
ror z80_l      ;- rr l
rts            ;- ret

