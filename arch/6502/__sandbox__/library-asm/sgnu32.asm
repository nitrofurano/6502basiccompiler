; Returns SGN (SIGN) for 32 bits unsigned integer
;
__SGNU32:
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
ora z80_d      ;- or d
ora z80_e      ;- or e
bne *+3        ;- ret z
rts
lda #1         ;- ld a,1
sta z80_a
rts            ;- ret

