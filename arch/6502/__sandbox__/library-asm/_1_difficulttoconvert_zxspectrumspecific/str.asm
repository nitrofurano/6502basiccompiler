; The STR$( ) BASIC function implementation
; Given a FP number in C ED LH
; Returns a pointer (in HL) to the memory heap
; containing the FP number string representation
;    ----- (using rom calculator??????)
#include once <alloc.asm>
#include once <stackf.asm>
#include once <const.asm>
;
__STR:
__STR_FAST:
                     ;- PROC
                     ;- LOCAL __STR_END
                     ;- LOCAL RECLAIM2
                     ;- LOCAL STK_END
                     ;- ld hl, (STK_END)
lda z80_l            ;- push hl; Stores STK_END
pha
lda z80_h
pha
                     ;- ld hl, (ATTR_T)	; Saves ATTR_T since it's changed by STR$ due to a ROM BUG
lda z80_l            ;- push hl
pha
lda z80_h
pha
jsr __FPSTACK_PUSH   ;- call __FPSTACK_PUSH ; Push number into stack
                     ;- rst 28h		; # Rom Calculator (???)
                     ;- defb 2Eh	; # STR$(x)
                     ;- defb 38h	; # END CALC
jsr __FPSTACK_POP    ;- call __FPSTACK_POP ; Recovers string parameters to A ED CB (Only ED LH are important)
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
                     ;- ld (ATTR_T), hl	; Restores ATTR_T
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
                     ;- ld (STK_END), hl	; Balance STK_END to avoid STR$ bug
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda z80_e            ;- push de
pha
lda z80_d
pha
                     ;- inc bc
                     ;- inc bc
jsr __MEM_ALLOC      ;- call __MEM_ALLOC ; HL Points to new block
pla                  ;- pop de
sta z80_d
pla
sta z80_e
                     ;- pop bc
lda z80_l            ;- push hl
pha
lda z80_h
pha
                     ;- ld a,h
                     ;- or l
                     ;- jr z,__STR_END  ; Return if NO MEMORY (NULL)
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda z80_e            ;- push de
pha
lda z80_d
pha
                     ;- ld (hl),c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
                     ;- ld (hl),b
                     ;- inc hl		; Copies length
lda z80_e            ;- ex de, hl	; HL = start of original string
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
                     ;- ldir	  	; Copies string content
pla                  ;- pop de		; Original (ROM-CALC) string
sta z80_d
pla
sta z80_e
                     ;- pop bc		; Original Length

__STR_END:
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
                     ;- inc bc
jsr RECLAIM2         ;- call RECLAIM2 ; Frees TMP Memory
                     ;- pop hl		  ; String result
rts                  ;- ret

RECLAIM2 EQU 19E8h
STK_END EQU 5C65h
                     ;- ENDP

