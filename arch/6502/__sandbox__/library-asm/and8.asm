; FASTCALL boolean and 8 version.
; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
; Performs 8bit and 8bit and returns the boolean
;
__AND8:
lda z80_a    ;- or a
ora z80_a
sta z80_a
bne *+3      ;- ret z
rts
lda z80_h    ;- ld a, h
sta z80_a
rts          ;- ret

