#include once <ftou32reg.asm>
;
__FTOF16REG:	; Converts a Float to 16.16 (32 bit) fixed point decimal
; Input FP number in A EDCB (A exponent, EDCB mantissa)
lda z80_a            ;- ld l,a     ; Saves exponent for later
sta z80_l
ora z80_d            ;- or d
ora z80_e            ;- or e
ora z80_b            ;- or b
ora z80_c            ;- or c
lda z80_e            ;- ld h,e
sta z80_h
bne *+3              ;- ret z		; Return if ZERO
rts
lda z80_l            ;- push hl  ; Stores it for later (Contains sign in H, exponent in L)
pha
lda z80_h
pha
lda z80_e            ;- push de
pha
lda z80_d
pha
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                  ;- pop de   ; Loads mantissa into C'B' E'D'
sta z80_d
pla
sta z80_e
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_c            ;- set 7,c ; Highest mantissa bit is always 1
ora #$80
sta z80_c
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda #0               ;- ld hl, 0 ; DEHL = 0
sta z80_l
lda #0
sta z80_h
lda z80_h            ;- ld d,h
sta z80_d
lda z80_l            ;- ld e,l
sta z80_e
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_c               ;- ld a,c  ; Get exponent
sta z80_a
sec                     ;- sub 112  ; Exponent -= 128 + 16
sbc #112
lda z80_c               ;- push bc  ; Saves sign in b again
pha
lda z80_b
pha
jeq __FTOU32REG_END     ;- jp z,__FTOU32REG_END	; If it was <= 128, we are done (Integers must be > 128)
jcc __FTOU32REG_END     ;- jp c,__FTOU32REG_END	; It was decimal (0.xxx). We are done (return 0)
lda z80_a               ;- ld b,a  ; Loop counter = exponent - 128 + 16 (we need to shift 16 bit more)
sta z80_b
jmp __FTOU32REG_LOOP    ;- jp __FTOU32REG_LOOP ; proceed as an u32 integer

