#include once <store32.asm>
;
; Stores a 32 bit integer number (DE,HL) at (IX + BC)
__PSTORE32:
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_l           ;- add hl,bc
clc
adc z80_c
sta z80_l
lda z80_h
adc z80_b
sta z80_h
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
jmp __STORE32        ;- jp __STORE32

