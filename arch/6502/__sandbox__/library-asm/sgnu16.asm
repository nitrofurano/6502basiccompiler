; Returns SGN (SIGN) for 16 bits unsigned integer
;
__SGNU16:
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
bne *+3        ;- ret z
rts
lda #1         ;- ld a,1
sta z80_a
rts            ;- ret

