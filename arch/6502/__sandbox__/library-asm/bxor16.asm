; vim:ts=4:et:
; FASTCALL bitwise xor 16 version.
; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
; Performs 16bit xor 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL XOR DE
;
__BXOR16:
lda z80_h      ;- ld a,h
sta z80_a
eor z80_d      ;- xor d
lda z80_a      ;- ld h,a
sta z80_h
lda z80_l      ;- ld a,l
sta z80_a
eor z80_e      ;- xor e
lda z80_a      ;- ld l,a
sta z80_l
rts            ;- ret

