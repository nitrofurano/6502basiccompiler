#include once <printnum.asm>
#include once <div8.asm>
;
__PRINTI8:	; Prints an 8 bits number in Accumulator (A)
; Converts 8 to 32 bits
ora z80_a            ;- or a
                     ;- jp p, __PRINTU8
pha                  ;- push af
php
jsr __PRINT_MINUS    ;- call __PRINT_MINUS
plp                  ;- pop af
pha
                     ;- neg

__PRINTU8:
                     ;- PROC
                     ;- LOCAL __PRINTU_LOOP
lda %0               ;- ld b,0 ; Counter
sta z80_b

__PRINTU_LOOP:
ora z80_a            ;- or a
jeq __PRINTU_START   ;- jp z, __PRINTU_START
lda z80_c            ;- push bc
pha
lda z80_b
pha
                     ;- ld h,10
jsr __DIVU8_FAST     ;- call __DIVU8_FAST ; Divides by 10. D'E'H'L' contains modulo (L' since < 10)
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_l            ;- ld a,l
sta z80_a
                     ;- or '0'		  ; Stores ASCII digit (must be print in reversed order)
pha                  ;- push af
php
lda z80_h            ;- ld a,h
sta z80_a
inc z80_b            ;- inc b
jmp __PRINTU_LOOP    ;- jp __PRINTU_LOOP ; Uses JP in loops
                     ;- ENDP

