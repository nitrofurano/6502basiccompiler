#include once <stackf.asm>
;
LN: ; Computes Ln(x) using ROM FP-CALC
jsr __FPSTACK_PUSH   ;- call __FPSTACK_PUSH
                     ;- rst 28h	; ROM CALC
.byte $20            ;- defb 20h ; 25h
.byte $38            ;- defb 38h ; END CALC
jmp __FPSTACK_POP    ;- jp __FPSTACK_POP

