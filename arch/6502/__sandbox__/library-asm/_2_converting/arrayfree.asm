; This routine is in charge of freeing an array of strings from memory
; HL = Pointer to start of array in memory
; Top of the stack = Number of elements of the array
;
#include once <free.asm>
;
__ARRAY_FREE:
                     ;- PROC
                     ;- LOCAL __ARRAY_LOOP
lda z80_e            ;- ex_de_hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla                  ;- pop hl		; (ret address)
sta z80_h
pla
sta z80_l
tsx                  ;- ex (sp), hl	; Callee -> HL = Number of elements
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h

__ARRAY_FREE_FAST:	; Fastcall entry: DE = Number of elements
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z		; ret if NULL
rts
lda z80_d            ;- ld b,d
sta z80_b
lda z80_e            ;- ld c,e
sta z80_c
ldy #$00             ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
inc z80_l            ;- inc hl		; DE = Number of dimensions
bne *+4
inc z80_h
lda z80_e            ;- ex_de_hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
asl z80_l            ;- add hl,hl	; HL = HL * 2
rol z80_h
clc                  ;- add hl,de
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
inc z80_l            ;- inc hl		; HL now points to the element start
bne *+4
inc z80_h

__ARRAY_LOOP:
ldy #$00             ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
inc z80_l            ;- inc hl		; DE = (HL) = String Pointer
bne *+4
inc z80_h
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
jsr __MEM_FREE       ;- call __MEM_FREE ; Frees it from memory
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
pla                      ;- pop hl
sta z80_h
pla
sta z80_l
                         ;- dec bc
lda z80_b                ;- ld a,b
sta z80_a
ora z80_c                ;- or c
jne __ARRAY_LOOP         ;- jp nz, __ARRAY_LOOP
rts                      ;- ret		    ; Frees it and return
                         ;- ENDP

__ARRAY_FREE_MEM: ; like the above, buf also frees the array itself
lda z80_e                 ;- ex_de_hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla                       ;- pop hl		; (ret address)
sta z80_h
pla
sta z80_l
tsx                       ;- ex (sp),hl	; Callee -> HL = Number of elements
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
lda z80_e                 ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda z80_l                ;- push hl		; Saves array pointer for later
pha
lda z80_h
pha
jsr __ARRAY_FREE_FAST    ;- call __ARRAY_FREE_FAST
pla                      ;- pop hl		; recovers array block pointer
sta z80_h
pla
sta z80_l
jmp __MEM_FREE           ;- jp __MEM_FREE	; Frees it and returns from __MEM_FREE

