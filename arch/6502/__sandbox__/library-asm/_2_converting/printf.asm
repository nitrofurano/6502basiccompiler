#include once <printstr.asm>
#include once <stackf.asm>
#include once <const.asm>
;
__PRINTF:	; Prints a Fixed point Number stored in C ED LH
PROC
LOCAL RECLAIM2
LOCAL STK_END
STK_END EQU	5C65h
                     ;- ld hl, (ATTR_T)
lda z80_l            ;- push hl ; Saves ATTR_T since BUG ROM changes it
pha
lda z80_h
pha
                     ;- ld hl, (STK_END)
lda z80_l            ;- push hl	; Stores STK_END
pha
lda z80_h
pha
jsr __FPSTACK_PUSH   ;- call __FPSTACK_PUSH ; Push number into stack
                     ;- rst 28h		; # Rom Calculator
                     ;- defb 2Eh	; # STR$(x)
                     ;- defb 38h	; # END CALC
jsr __FPSTACK_POP    ;- call __FPSTACK_POP ; Recovers string parameters to A ED CB
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
                     ;- ld (STK_END),hl ; Balance STK_END to avoid STR$ bug
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
                     ;- ld (ATTR_T),hl	 ; Restores ATTR_T
lda z80_e            ;- ex de,hl	; String position now in HL
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda z80_c            ;- push bc
pha
lda z80_b
pha
eor z80_a            ;- xor a       ; Avoid the str to be FREED from heap
jsr __PRINT_STR      ;- call __PRINT_STR
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
inc z80_c            ;- inc bc
bne *+4
inc z80_b
jmp RECLAIM2         ;- jp RECLAIM2 ; Frees TMP Memory
                     ;- RECLAIM2 EQU 19E8h
                     ;- ENDP

