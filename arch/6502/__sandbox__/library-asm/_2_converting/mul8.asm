__MUL8:		; Performs 8bit x 8bit multiplication
;PROC
;LOCAL __MUL8A
;LOCAL __MUL8LOOP
;LOCAL __MUL8B
; 1st operand (byte) in A, 2nd operand into the stack (AF)
pla                ;- pop hl	; return address
sta z80_h
pla
sta z80_l
tsx                ;- ex (sp), hl ; CALLE convention
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x

;;__MUL8_FAST: ; __FASTCALL__ entry
;;	ld e, a
;;	ld d, 0
;;	ld l, d
;;	sla h
;;	jr nc, __MUL8A
;;	ld l, e
;;__MUL8A:
;;	ld b, 7
;;__MUL8LOOP:
;;	add hl, hl
;;	jr nc, __MUL8B
;;	add hl, de
;;__MUL8B:
;;	djnz __MUL8LOOP
;;	ld a, l ; result = A and HL  (Truncate to lower 8 bits)

__MUL8_FAST: ; __FASTCALL__ entry, a = a * h (8 bit mul) and Carry
lda %8               ;- ld b,8
sta z80_b
lda z80_a            ;- ld l,a
sta z80_l
eor z80_a            ;- xor a

__MUL8LOOP:
clc                  ;- add a,a     ; a *= 2
adc z80_a
asl z80_l            ;- sla l
jcs __MUL8B          ;- jp nc, __MUL8B
clc                  ;- add a,h
adc z80_h

__MUL8B:
                     ;- djnz __MUL8LOOP
rts                  ;- ret		; result = HL
                     ;- ENDP

