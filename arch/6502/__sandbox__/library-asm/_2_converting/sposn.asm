; Printing positioning library.
                     ;- PROC
                     ;- LOCAL ECHO_E

__LOAD_S_POSN:		; Loads into DE current ROW, COL print position from S_POSN mem var.
lda S_POSN           ;- ld de,(S_POSN)
sta z80_e
lda S_POSN+1
sta z80_d
lda MAXX             ;- ld hl,(MAXX)
sta z80_l
lda MAXX+1
sta z80_h
ora z80_a            ;- or a
lda z80_l            ;- sbc hl,de
sbc z80_e
sta z80_l
lda z80_h
sbc z80_d
sta z80_h
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
rts                  ;- ret

__SAVE_S_POSN:		; Saves ROW, COL from DE into S_POSN mem var.
lda MAXX             ;- ld hl,(MAXX)
sta z80_l
lda MAXX+1
sta z80_h
ora z80_a            ;- or a
lda z80_l            ;- sbc hl,de
sbc z80_e
sta z80_l
lda z80_h
sbc z80_d
sta z80_h
                     ;- ld (S_POSN), hl ; saves it again
rts                  ;- ret

ECHO_E	EQU 23682
MAXX	EQU ECHO_E   ; Max X position + 1
MAXY	EQU MAXX + 1 ; Max Y position + 1
S_POSN	EQU 23688
POSX	EQU S_POSN		; Current POS X
POSY	EQU S_POSN + 1	; Current POS Y
ENDP

