#include once <print.asm>
#include once <sposn.asm>
#include once <attr.asm>
#include once <free.asm>
;
; PRINT command routine
; Prints string pointed by HL
;
PRINT_STR:
__PRINTSTR:		; __FASTCALL__ Entry to print_string
                     ;- PROC
                     ;- LOCAL __PRINT_STR_LOOP
                     ;- LOCAL __PRINT_STR_END
lda z80_a            ;- ld d,a ; Saves A reg (Flag) for later
sta z80_d
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z	; Return if the pointer is NULL
rts
lda z80_l            ;- push hl
pha
lda z80_h
pha
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld b,(hl)
lda (z80_hl),y
sta z80_b
inc z80_l            ;- inc hl	; BC = LEN(a$); HL = &a$
bne *+4
inc z80_h

__PRINT_STR_LOOP:
lda z80_b            ;- ld a,b
sta z80_a
ora z80_c            ;- or c
jeq __PRINT_STR_END  ;- jr z, __PRINT_STR_END 	; END if BC (counter = 0)
ldy #$00             ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
jsr __PRINTCHAR      ;- call __PRINTCHAR
inc z80_l            ;- inc hl
bne *+4
inc z80_h
                     ;- dec bc
jmp __PRINT_STR_LOOP ;- jp __PRINT_STR_LOOP

__PRINT_STR_END:
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_d            ;- ld a,d ; Recovers A flag
sta z80_a
ora z80_a            ;- or a   ; If not 0 this is a temporary string. Free it
bne *+3              ;- ret z
rts
jmp __MEM_FREE       ;- jp __MEM_FREE ; Frees str from heap and return from there

__PRINT_STR:
; Fastcall Entry
; It ONLY prints strings
; HL = String start
; BC = String length (Number of chars)
lda z80_l              ;- push hl ; Push str address for later
pha
lda z80_h
pha
lda z80_a              ;- ld d,a ; Saves a FLAG
sta z80_d
jmp __PRINT_STR_LOOP   ;- jp __PRINT_STR_LOOP
                       ;- ENDP

