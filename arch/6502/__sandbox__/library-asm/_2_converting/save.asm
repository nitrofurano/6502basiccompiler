; Save code "XXX" at address YYY of length ZZZ
; Parameters in the stack are XXX (16 bit) address of string name
; (only first 12 chars will be taken into account)
; YYY and ZZZ are 16 bit on top of the stack.
;
#include once <error.asm>
;
SAVE_CODE:
                     ;- PROC
                     ;- LOCAL MEMBOT
                     ;- LOCAL SAVE_CONT
                     ;- LOCAL ROM_SAVE
                     ;- LOCAL __ERR_EMPTY
                     ;- LOCAL SAVE_STOP
                     ;- ROM_SAVE EQU 0970h
MEMBOT EQU 23698 ; Use the CALC mem to store header
                     ;- pop hl   ; Return address
                     ;- pop bc     ; data length in bytes
                     ;- pop de   ; address start
                     ;- ex (sp), hl ; CALLE => now hl = String
; This function will call the ROM SAVE CODE Routine
; Parameters in the stack are HL => String with SAVE name
; (only first 12 chars will be taken into account)
; DE = START address of CODE to save
; BC = Length of data in bytes

__SAVE_CODE: ; INLINE version
                     ;- ld a, b
ora z80_c            ;- or c
                     ;- ret z    ; Return if block length == 0
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
                     ;- jr z, __ERR_EMPTY  ; Return if NULL STRING
                     ;- ld ix, MEMBOT
                     ;- ld (ix+00),3 ; CODE
                     ;- ld (ix+11),c
                     ;- ld (ix+12),b ; Store long in bytes
                     ;- ld (ix+13),e
                     ;- ld (ix+14),d ; Store address in bytes
lda z80_l            ;- push hl
pha
lda z80_h
pha
                     ;- ld bc, 9
                     ;- ld hl,MEMBOT + 1
                     ;- ld de,MEMBOT + 2
                     ;- ld (hl), ' '
                     ;- ldir   ; Fill the filename with blanks
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld b,(hl)
lda (z80_hl),y
sta z80_b
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_b            ;- ld a,b
sta z80_a
ora z80_c            ;- or c

__ERR_EMPTY:
                     ;- ld a, ERROR_InvalidFileName
jeq SAVE_STOP        ;- jr z, SAVE_STOP        ; Return if str len == 0
lda z80_e            ;- ex de, hl  ; Saves HL in DE
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda #<10             ;- ld hl, 10
sta z80_l
lda #>10
sta z80_h
ora z80_a            ;- or a
                     ;- sbc hl, bc  ; Test BC > 10?
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
                     ;- jr nc,SAVE_CONT ; Ok BC <= 10
                     ;- ld bc,10 ; BC at most 10 chars

SAVE_CONT:
                     ;- ld de, MEMBOT + 1
                     ;- ldir     ; Copy String block NAME
                     ;- ld l,(ix+13)
                     ;- ld h,(ix+14)    ; Restores start of bytes
jsr ROM_SAVE         ;- call ROM_SAVE
; Recovers ECHO_E since ROM SAVE changes it
                     ;- ld hl,1821h
                     ;- ld (23682),hl
pla                  ;- pop ix
sta z80_ix+1
pla
sta z80_ix
rts                  ;- ret

SAVE_STOP:
pla                  ;- pop ix
sta z80_ix+1
pla
sta z80_ix
jmp __STOP           ;- jp __STOP
                     ;- ENDP


