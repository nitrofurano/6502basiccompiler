#include once <printnum.asm>
#include once <neg32.asm>
#include once <div32.asm>
#include once <attr.asm>
;
__PRINTI32:
lda z80_d            ;- ld a,d
sta z80_a
ora z80_a            ;- or a
                     ;- jp p, __PRINTU32
jsr __PRINT_MINUS    ;- call __PRINT_MINUS
jsr __NEG32          ;- call __NEG32

__PRINTU32:
                     ;- PROC
                     ;- LOCAL __PRINTU_LOOP
lda %0               ;- ld b,0 ; Counter
sta z80_b

__PRINTU_LOOP:
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
ora z80_d            ;- or d
ora z80_e            ;- or e
jeq __PRINTU_START   ;- jp z, __PRINTU_START
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda #<0              ;- ld bc,0
sta z80_c
lda #>0
sta z80_b
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda #<10             ;- ld bc,10
sta z80_c
lda #>10
sta z80_b
lda z80_c            ;- push bc		  ; Push 00 0A (10 Dec) into the stack = divisor
pha
lda z80_b
pha
jsr __DIVU32         ;- call __DIVU32 ; Divides by 32. D'E'H'L' contains modulo (L' since < 10)
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_l            ;- ld a,l
sta z80_a
                     ;- or '0'		  ; Stores ASCII digit (must be print in reversed order)
pha                  ;-  push af
php
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
inc z80_b            ;- inc b
jmp __PRINTU_LOOP    ;- jp __PRINTU_LOOP ; Uses JP in loops
                     ;- ENDP

