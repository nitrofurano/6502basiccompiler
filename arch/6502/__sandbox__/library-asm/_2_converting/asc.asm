; Returns the ascii code for the given str
#include once <free.asm>
;
__ASC:
                     ;- PROC
                     ;- LOCAL __ASC_END
ldx z80_ap           ;- ex af,af'	; Saves free_mem flag
sta z80_ap
txa
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z		; NULL? return
rts
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld b,(hl)
lda (z80_hl),y
sta z80_b
lda z80_b            ;- ld a,b
sta z80_a
ora z80_c            ;- or c
jeq __ASC_END        ;- jr z, __ASC_END		; No length? return
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
                     ;- dec hl

__ASC_END:
                     ;- dec hl
ldx z80_ap           ;- ex af,af'
sta z80_ap
txa
ora z80_a            ;- or a
beq *+5              ;- call nz, __MEM_FREE	; Free memory if needed
jsr __MEM_FREE
ldx z80_ap           ;- ex af,af'	; Recover result
sta z80_ap
txa
rts                  ;- ret
                     ;- ENDP
