#include once <alloc.asm>
; Loads a string (ptr) from HL
; and duplicates it on dynamic memory again
; Finally, it returns result pointer in HL
;
__ILOADSTR:		; This is the indirect pointer entry HL = (HL)
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z
rts
ldy #$00             ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_a            ;- ld l,a
sta z80_l

__LOADSTR:		; __FASTCALL__ entry
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z	; Return if NULL
rts
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld b,(hl)
lda (z80_hl),y
sta z80_b
                     ;- dec hl  ; BC = LEN(a$)
inc z80_c            ;- inc bc
bne *+4
inc z80_b
inc z80_c            ;- inc bc	; BC = LEN(a$) + 2 (two bytes for length)
bne *+4
inc z80_b
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_c            ;- push bc
pha
lda z80_b
pha
jsr __MEM_ALLOC      ;- call __MEM_ALLOC
pla                  ;- pop bc  ; Recover length
sta z80_b
pla
sta z80_c
pla                  ;- pop de  ; Recover origin
sta z80_d
pla
sta z80_e
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z	; Return if NULL (No memory)
rts
lda z80_e            ;- ex de, hl ; ldir takes HL as source, DE as destiny, so SWAP HL,DE
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda z80_e            ;- push de	; Saves destiny start
pha
lda z80_d
pha
                     ;- ldir	; Copies string (length number included)
pla                  ;- pop hl	; Recovers destiny in hl as result
sta z80_h
pla
sta z80_l
rts                  ;- ret

