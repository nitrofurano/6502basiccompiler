; INKEY Function
; Returns a string allocated in dynamic memory
; containing the string.
; An empty string otherwise.
;
#include once <alloc.asm>
;
INKEY:
PROC
LOCAL __EMPTY_INKEY
LOCAL KEY_SCAN
LOCAL KEY_TEST
LOCAL KEY_CODE
                     ;- ld bc,3        ; 1 char length string
jsr __MEM_ALLOC      ;- call __MEM_ALLOC
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z	; Return if NULL (No memory)
rts
lda z80_l            ;- push hl ; Saves memory pointer
pha
lda z80_h
pha
jsr KEY_SCAN         ;- call KEY_SCAN
jne __EMPTY_INKEY    ;- jp nz, __EMPTY_INKEY
jsr KEY_TEST         ;- call KEY_TEST
jcs __EMPTY_INKEY    ;- jp nc, __EMPTY_INKEY
                     ;- dec d	; D is expected to be FLAGS so set bit 3 $FF
; 'L' Mode so no keywords.
lda z80_a            ;- ld e,a    ; main key to A
sta z80_e
; C is MODE 0 'KLC' from above still.
jsr KEY_CODE         ;- call KEY_CODE ; routine K-DECODE
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda #1               ;- ld (hl),1
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda #0               ;- ld (hl),0
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
                     ;- dec hl
                     ;- dec hl	; HL Points to string result
rts                  ;- ret

__EMPTY_INKEY:
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
eor z80_a            ;- xor a
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
                     ;- dec hl
rts                  ;- ret
;
KEY_SCAN	EQU 028Eh
KEY_TEST	EQU 031Eh
KEY_CODE	EQU 0333h
ENDP

