#include once <neg32.asm>
__I8TOFREG:
lda z80_a            ;- ld l,a
sta z80_l
                     ;- rlca
                     ;- sbc a, a ; A = SGN(A)
lda z80_a            ;- ld h,a
sta z80_h
lda z80_a            ;- ld e,a
sta z80_e
lda z80_a            ;- ld d,a
sta z80_d

__I32TOFREG: ; Converts a 32bit signed integer (stored in DEHL)
; to a Floating Point Number returned in (A ED CB)
lda z80_d            ;- ld a,d
sta z80_a
                     ;- or a  ; Test sign
                     ;- jp p, __U32TOFREG ; It was positive, proceed as 32bit unsigned
jsr __NEG32          ;- call __NEG32  ; Convert it to positive
jsr __U32TOFREG      ;- call __U32TOFREG ; Convert it to Floating point
                     ;- set 7, e   ; Put the sign bit (negative) in the 31bit of mantissa
rts                  ;- ret

__U8TOFREG:
; Converts an unsigned 8 bit (A) to Floating point
lda z80_a            ;- ld l,a
sta z80_l
                     ;- ld h, 0
lda z80_h            ;- ld e,h
sta z80_e
lda z80_h            ;- ld d,h
sta z80_d

__U32TOFREG: ; Converts an unsigned 32 bit integer (DEHL)
; to a Floating point number returned in A ED CB
                     ;- PROC
                     ;- LOCAL __U32TOFREG_END
lda z80_d            ;- ld a,d
sta z80_a
ora z80_e            ;- or e
ora z80_h            ;- or h
ora z80_l            ;- or l
lda z80_d            ;- ld b,d
sta z80_b
lda z80_e            ;- ld c,e  ; Returns 00 0000 0000 if ZERO
sta z80_c
bne *+3              ;- ret z
rts
lda z80_e            ;- push de
pha
lda z80_d
pha
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                  ;- pop de  ; Loads integer into B'C' D'E'
sta z80_d
pla
sta z80_e
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
                     ;- ld l,128 ; Exponent
                     ;- ld bc,0 ; DEBC = 0
lda z80_b            ;- ld d,b
sta z80_d
lda z80_c            ;- ld e,c
sta z80_e

__U32TOFREG_LOOP: ; Also an entry point for __F16TOFREG
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_d            ;- ld a,d  ; B'C'D'E' == 0 ?
sta z80_a
ora z80_e            ;- or e
ora z80_b            ;- or b
ora z80_c            ;- or c
jeq __U32TOFREG_END  ;- jp z, __U32TOFREG_END ; We are done
                     ;- srl b ; Shift B'C' D'E' >> 1, output bit stays in Carry
ror z80_c            ;- rr c
ror z80_d            ;- rr d
ror z80_e            ;- rr e
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
ror z80_e            ;- rr e ; Shift EDCB >> 1, inserting the carry on the left
ror z80_d            ;- rr d
ror z80_c            ;- rr c
ror z80_b            ;- rr b
inc z80_l            ;- inc l ; Increment exponent
jmp __U32TOFREG_LOOP ;- jp __U32TOFREG_LOOP

__U32TOFREG_END:
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_l            ;- ld a,l     ; Puts the exponent in a
sta z80_a
lda z80_e            ;- res 7, e ; Sets the sign bit to 0 (positive)
and #$FF-$80
sta z80_e
rts                  ;- ret
                     ;- ENDP

