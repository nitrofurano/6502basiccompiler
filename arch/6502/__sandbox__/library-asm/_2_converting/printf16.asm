#include once <printnum.asm>
#include once <printi16.asm>
#include once <neg32.asm>
;
__PRINTF16:	; Prints a 32bit 16.16 fixed point number
PROC
LOCAL __PRINT_FIX_LOOP
LOCAL __PRINTF16_2
                     ;- bit 7, d
                     ;- jr z, __PRINTF16_2
jsr __NEG32          ;- call __NEG32
jsr __PRINT_MINUS    ;- call __PRINT_MINUS

__PRINTF16_2:
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
jsr __PRINTU16       ;- call __PRINTU16 ; Prints integer part
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
                     ;- ret z		; Returns if integer
lda z80_l            ;- push hl
pha
lda z80_h
pha
                     ;- ld a, '.'
jsr __PRINT_DIGIT    ;- call __PRINT_DIGIT	; Prints decimal point
pla                  ;- pop hl
sta z80_h
pla
sta z80_l

__PRINT_FIX_LOOP:
lda z80_h            ;- ld a,h
sta z80_a
ora z80_l            ;- or l
bne *+3              ;- ret z		; Returns if no more decimals
rts
eor z80_a            ;- xor a
lda z80_h            ;- ld d,h
sta z80_d
lda z80_l            ;- ld e,l
sta z80_e
; Fast NUM * 10 multiplication
asl z80_l            ;- add hl,hl
rol z80_h
                     ;- adc a,a    ; AHL = AHL * 2  (= X * 2)
asl z80_l            ;- add hl,hl
rol z80_h
                     ;- adc a,a    ; AHL = AHL * 2  (= X * 4)
clc                  ;- add hl,de
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
                     ;- adc a,0    ; AHL = AHL + DE (= X * 5)
asl z80_l            ;- add hl,hl
rol z80_h
                     ;- adc a,a    ; AHL = AHL * 2 (= X * 10)
lda z80_l            ;- push hl
pha
lda z80_h
pha
                     ;- or '0'
jsr __PRINT_DIGIT    ;- call __PRINT_DIGIT
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
jmp __PRINT_FIX_LOOP ;- jp __PRINT_FIX_LOOP
                     ;- ENDP

