#include once <sposn.asm>
#include once <error.asm>
;
__IN_SCREEN:
; Returns NO carry if current coords (D, E)
; are OUT of the screen limits (MAXX, MAXY)
PROC
LOCAL __IN_SCREEN_ERR
lda #<MAXX           ;- ld hl,MAXX
sta z80_l
lda #>MAXX
sta z80_h
lda z80_e            ;- ld a,e
sta z80_a
ldy #$00             ;- cp (hl)
cmp (z80_hl),y
jcs __IN_SCREEN_ERR  ;- jr nc, __IN_SCREEN_ERR	; Do nothing and return if out of range
lda z80_d            ;- ld a,d
sta z80_a
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- cp (hl)
cmp (z80_hl),y
;; jr nc, __IN_SCREEN_ERR	; Do nothing and return if out of range
;; ret
bcs *+3              ;- ret c  ; Return if carry (OK)
rts

__IN_SCREEN_ERR:
__OUT_OF_SCREEN_ERR:
; Jumps here if out of screen
                     ;- ld a, ERROR_OutOfScreen
jmp __STOP           ;- jp __STOP   ; Saves error code and exits
                     ;- ENDP

