__MUL16:	; Mutiplies HL with the last value stored into de stack
; Works for both signed and unsigned
               ;- PROC
               ;- LOCAL __MUL16LOOP
               ;- LOCAL __MUL16NOADD
lda z80_e      ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla            ;- pop hl		    ; Return address
sta z80_h
pla
sta z80_l
tsx            ;- ex (sp),hl    ; CALLEE caller convention
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x

;;__MUL16_FAST:	; __FASTCALL ENTRY: HL = 1st operand, DE = 2nd Operand
;;		ld c, h
;;		ld a, l	 ; C,A => 1st Operand
;;
;;		ld hl, 0 ; Accumulator
;;		ld b, 16
;;
;;__MUL16LOOP:
;;		sra c	; C,A >> 1  (Arithmetic)
;;		rra
;;
;;		jr nc, __MUL16NOADD
;;		add hl, de
;;
;;__MUL16NOADD:
;;		sla e
;;		rl d
;;
;;		djnz __MUL16LOOP

__MUL16_FAST:
lda %16              ;- ld b,16
sta z80_b
lda z80_d            ;- ld a,d
sta z80_a
lda z80_e            ;- ld c,e
sta z80_c
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda #<0              ;- ld hl,0
sta z80_l
lda #>0
sta z80_h

__MUL16LOOP:
asl z80_l            ;- add hl,hl  ; hl << 1
rol z80_h
                     ;- sla c
                     ;- rla         ; a,c << 1
jcs __MUL16NOADD     ;- jp nc, __MUL16NOADD
clc                  ;- add hl,de
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h

__MUL16NOADD:
                     ;- djnz __MUL16LOOP
rts                  ;- ret	; Result in hl (16 lower bits)
                     ;- ENDP

