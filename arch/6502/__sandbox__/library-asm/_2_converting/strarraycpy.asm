; (K)opyleft - by Jose M. Rodriguez de la Rosa (a.k.a. Boriel)
; 2009 - This is Free OpenSource BSD code
; vim: et:ts=4:sw=4
; Copies a vector of strings from one place to another
; reallocating strings of the destiny vector to hold source strings.
; This is used in the following code:
; DIM a$(20) : DIM b$(20): a$ = b$
;
#include once <lddede.asm>
#include once <strcpy.asm>
;
STR_ARRAYCOPY:
; Copies an array of string a$ = b$
; Parameters in the stack:
; a$, b$, num. of elements;
pla                  ;- pop hl  ; ret address
sta z80_h
pla
sta z80_l
pla                  ;- pop bc  ; num of elements
sta z80_b
pla
sta z80_c
pla                  ;- pop de  ; source array + offset to the 1st elem.
sta z80_d
pla
sta z80_e
tsx                  ;- ex (sp), hl ; Calle -> hl = destiny array + offset to the 1st elem.
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x

; FASTCALL ENTRY
; HL = a$ + offset
; DE = b$ + offset
; BC = Number of elements
;
__STR_ARRAYCOPY:
                     ;- PROC
                     ;- LOCAL LOOP

LOOP:
lda z80_b            ;- ld a,b
sta z80_a
ora z80_c            ;- or c
bne *+3              ;- ret z ; Done!
rts
                     ;- dec bc
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda z80_e            ;- push de
pha
lda z80_d
pha
ldy #$00             ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
                     ;- dec hl
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_c            ;- ld h,c
sta z80_h
lda z80_a            ;- ld l,a
sta z80_l
jsr __LOAD_DE_DE     ;- call __LOAD_DE_DE
jsr __STRASSIGN      ;- call __STRASSIGN
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_e            ;- ld (hl),e
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_d            ;- ld (hl),d
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
pla                  ;- pop de
sta z80_d
pla
sta z80_e
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
inc z80_e            ;- inc de
bne *+4
inc z80_d
inc z80_e            ;- inc de
bne *+4
inc z80_d
jmp LOOP             ;- jp LOOP
                     ;- ENDP

