; Computes A % B for fixed values
;
#include once <divf16.asm>
#include once <mulf16.asm>
;
__MODF16:
; 16.16 Fixed point Division (signed)
; DE.HL = Divisor, Stack Top = Divider
; A = Dividend, B = Divisor => A % B
PROC
LOCAL TEMP
TEMP EQU 23698       ; MEMBOT
pla                  ;- pop bc              ; ret addr
sta z80_b
pla
sta z80_c
lda z80_c            ;- ld (TEMP),bc       ; stores it on MEMBOT temporarily
sta TEMP
lda z80_b
sta TEMP+1
lda z80_l            ;- ld (TEMP+2),hl   ; stores HP of divider
sta TEMP+2
lda z80_h
sta TEMP+2+1
lda z80_e            ;- ld (TEMP+4),de   ; stores DE of divider
sta TEMP+4
lda z80_d
sta TEMP+4+1
jsr __DIVF16         ;- call __DIVF16
                     ;- rlc d				; Sign into carry
                     ;- sbc a, a			; a register = -1 sgn(DE), or 0
lda z80_a            ;- ld d,a
sta z80_d
lda z80_a            ;- ld e,a				; DE = 0 if it was positive or 0; -1 if it was negative
sta z80_e
lda TEMP+4           ;- ld bc,(TEMP + 4)	; Pushes original divider into the stack
sta z80_c
lda TEMP+4+1
sta z80_b
lda z80_c            ;- push bc
pha
lda z80_b
pha
lda TEMP+2           ;- ld bc,(TEMP+2)
sta z80_c
lda TEMP+2+1
sta z80_b
lda z80_c            ;- push bc
pha
lda z80_b
pha
                     ;- ld bc, (TEMP)    ; recovers return address
lda z80_c            ;- push bc
pha
lda z80_b
pha
jmp __MULF16         ;- jp __MULF16			; multiplies and return from there
                     ;- ENDP

