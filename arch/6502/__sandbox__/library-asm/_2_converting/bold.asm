; Sets BOLD flag in P_FLAG permanently
; Parameter: BOLD flag in bit 0 of A register
#include once <copy_attr.asm>
;
BOLD:
                     ;- PROC
and #1               ;- and 1
                     ;- rlca
                     ;- rlca
                     ;- rlca
lda #<FLAGS2         ;- ld hl,FLAGS2
sta z80_l
lda #>FLAGS2
sta z80_h
ldy #$00             ;- res 3,(hl)
lda (z80_hl),y
and #$F7
sta (z80_hl),y
ldy #$00             ;- or (hl)
ora (z80_hl),y
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
rts                  ;- ret

; Sets BOLD flag in P_FLAG temporarily
BOLD_TMP:
and #1               ;- and 1
                     ;- rlca
                     ;- rlca
lda #<FLAGS2         ;- ld hl,FLAGS2
sta z80_l
lda #>FLAGS2
sta z80_h
ldy #$00             ;- res 2,(hl)
lda (z80_hl),y
and #$FB
sta (z80_hl),y
ldy #$00             ;- or (hl)
ora (z80_hl),y
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
rts                  ;- ret
                     ;- ENDP

