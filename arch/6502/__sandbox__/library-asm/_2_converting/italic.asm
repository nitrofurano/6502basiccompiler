; Sets ITALIC flag in P_FLAG permanently
;- (isn’t it oblique or slanted instead? italic is a totally different thing...)
; Parameter: ITALIC flag in bit 0 of A register
#include once <copy_attr.asm>
;
ITALIC:
                     ;- PROC
and #1               ;- and 1
                     ;- rrca
                     ;- rrca
                     ;- rrca
lda #<FLAGS2         ;- ld hl,FLAGS2
sta z80_l
lda #>FLAGS2
sta z80_h
ldy #$00             ;- res 5,(hl)
lda (z80_hl),y
and #$DF
sta (z80_hl),y
ldy #$00             ;- or (hl)
ora (z80_hl),y
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
rts                  ;- ret

; Sets ITALIC flag in P_FLAG temporarily
ITALIC_TMP:
and #1               ;- and 1
                     ;- rrca
                     ;- rrca
                     ;- rrca
                     ;- rrca
lda #<FLAGS2         ;- ld hl,FLAGS2
sta z80_l
lda #>FLAGS2
sta z80_h
ldy #$00             ;- res 4,(hl)
lda (z80_hl),y
and #$EF
sta (z80_hl),y
ldy #$00             ;- or (hl)
ora (z80_hl),y
lda z80_a            ;- ld (hl),a
ldy #$00
sta (z80_hl),y
rts                  ;- ret
                     ;- ENDP

