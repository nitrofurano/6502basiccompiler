; Similar to __STORE_STR, but this one is called when
; the value of B$ if already duplicated onto the stack.
; So we needn't call STRASSING to create a duplication
; HL = address of string memory variable
; DE = address of 2n string. It just copies DE into (HL)
; 	freeing (HL) previously.
;
#include once <free.asm>
;
__PISTORE_STR2: ; Indirect store temporary string at (IX + BC)
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_l            ;- add hl,bc
clc
adc z80_c
sta z80_l
lda z80_h
adc z80_b
sta z80_h

__ISTORE_STR2:
ldy #$00             ;- ld c,(hl)  ; Dereferences HL
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_c            ;- ld l,c		; HL = *HL (real string variable address)
sta z80_l

__STORE_STR2:
lda z80_l            ;- push hl
pha
lda z80_h
pha
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_c            ;- ld l,c		; HL = *HL (real string address)
sta z80_l
lda z80_e            ;- push de
pha
lda z80_d
pha
jsr __MEM_FREE       ;- call __MEM_FREE
pla                  ;- pop de
sta z80_d
pla
sta z80_e
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_e            ;- ld (hl),e
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_d            ;- ld (hl),d
ldy #$00
sta (z80_hl),y
                     ;- dec hl		; HL points to mem address variable. This might be useful in the future.
rts                  ;- ret

