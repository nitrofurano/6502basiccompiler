__LTI8: ; Test 8 bit values A < H
; Returns result in A: 0 = False, !0 = True
sec            ;- sub h
sbc z80_h
__LTI:  ; Signed CMP
               ;- PROC
               ;- LOCAL __PE
lda #0         ;- ld a,0  ; Sets default to false
__LTI2:
               ;- jp pe, __PE
; Overflow flag NOT set
bmi *+3        ;- ret p
rts
sec            ;- dec a ; TRUE
sbc #$01

__PE:   ; Overflow set
bpl *+3        ;- ret m
rts
sec            ;- dec a ; TRUE
sbc #$01
rts            ;- ret
               ;- ENDP

