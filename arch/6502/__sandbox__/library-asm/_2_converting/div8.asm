; --------------------------------
__DIVU8:  ; 8 bit unsigned integer division
; Divides (Top of stack, High Byte) / A
                   ;- pop hl  ; --------------------------------
                   ;- ex (sp), hl  ; CALLEE

__DIVU8_FAST:  ; Does A / H
lda z80_h          ;- ld l,h
sta z80_l
lda z80_a          ;- ld h,a    ; At this point do H / L
sta z80_h
lda %8             ;- ld b,8
sta z80_b
eor z80_a          ;- xor a    ; A = 0, Carry Flag = 0

__DIV8LOOP:
asl z80_h          ;- sla h
                   ;- rla
cmp z80_l          ;- cp l
jcc __DIV8NOSUB    ;- jr c,__DIV8NOSUB
sec                ;- sub l
sbc z80_l
inc z80_h          ;- inc h

__DIV8NOSUB:
dec z80_b          ;- djnz __DIV8LOOP
jne __DIV8LOOP
lda z80_a          ;- ld l,a    ; save remainder
sta z80_l
lda z80_h          ;- ld a,h    ;
sta z80_a
rts                ;- ret      ; a = Quotient,

; --------------------------------
__DIVI8:    ; 8 bit signed integer division Divides (Top of stack) / A
                   ;- pop hl
; --------------------------------
tsx                ;- ex (sp),hl
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x

__DIVI8_FAST:
lda z80_a          ;- ld e,a    ; store operands for later
sta z80_e
lda z80_h          ;- ld c,h
sta z80_c
ora z80_a          ;- or a    ; negative?
jpl __DIV8A        ;- jp p, __DIV8A
eor #$ff           ;- neg      ; Make it positive
clc
adc #$01

__DIV8A:
ldx z80_ap         ;- ex af,af'
sta z80_ap
txa
lda z80_h          ;- ld a,h
sta z80_a
ora z80_a          ;- or a
jpl __DIV8B        ;- jp p, __DIV8B
eor #$ff           ;- neg
clc
adc #$01
lda z80_a          ;- ld h,a    ; make it positive
sta z80_h

__DIV8B:
ldx z80_ap            ;- ex af,af'
sta z80_ap
txa
jsr __DIVU8_FAST      ;- call __DIVU8_FAST
lda z80_c             ;- ld a,c
sta z80_a
eor z80_l             ;- xor l    ; bit 7 of A = 1 if result is negative
lda z80_h             ;- ld a,h    ; Quotient
sta z80_a
bmi *+3               ;- ret p    ; return if positive
rts
eor #$ff              ;- neg
clc
adc #$01
rts                   ;- ret

__MODU8:    ; 8 bit module. REturns A mod (Top of stack) (unsigned operands)
pla                   ;- pop hl
sta z80_h
pla
sta z80_l
tsx                   ;- ex (sp),hl  ; CALLEE
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x

__MODU8_FAST:  ; __FASTCALL__ entry
jsr __DIVU8_FAST        ;- call __DIVU8_FAST
lda z80_l               ;- ld a,l    ; Remainder
sta z80_a
rts                     ;- ret    ; a = Modulus

__MODI8:    ; 8 bit module. REturns A mod (Top of stack) (For singed operands)
pla                     ;- pop hl
sta z80_h
pla
sta z80_l
tsx                     ;- ex (sp),hl  ; CALLEE
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x

__MODI8_FAST:           ; __FASTCALL__ entry
jsr __DIVI8_FAST        ;- call __DIVI8_FAST
lda z80_l               ;- ld a,l    ; remainder
sta z80_a
rts                     ;- ret    ; a = Modulus

