; FASTCALL boolean xor 8 version.
; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
; Performs 32bit xor 32bit and returns the boolean
;
#include once <xor8.asm>
;
__XOR32:
lda z80_h     ;- ld a,h
sta z80_a
ora z80_l     ;- or l
ora z80_d     ;- or d
ora z80_e     ;- or e
lda z80_a     ;- ld c,a
sta z80_c
pla           ;- pop hl  ; RET address
sta z80_h
pla
sta z80_l
pla           ;- pop de
sta z80_d
pla
sta z80_e
tsx           ;- ex (sp),hl
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
lda z80_h     ;- ld a,h
sta z80_a
ora z80_l     ;- or l
ora z80_d     ;- or d
ora z80_e     ;- or e
lda z80_c     ;- ld h,c
sta z80_h
jmp __XOR8    ;- jp __XOR8

