; Returns SGN (SIGN) for 8 bits unsigned integera
;
__SGNU8:
ora z80_a      ;- or a
bne *+3        ;- ret z
rts
lda #1         ;- ld a,1
sta z80_a
rts            ;- ret

