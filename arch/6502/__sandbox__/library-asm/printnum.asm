#include once <print.asm>
#include once <attr.asm>
;
__PRINTU_START:
                     ;- PROC
                     ;- LOCAL __PRINTU_CONT
lda z80_b            ;- ld a,b
sta z80_a
ora z80_a            ;- or a
jne __PRINTU_CONT    ;- jp nz, __PRINTU_CONT
lda #'0'             ;- ld a,'0'
jmp __PRINT_DIGIT    ;- jp __PRINT_DIGIT

__PRINTU_CONT:
plp                  ;- pop af
pha
lda z80_c            ;- push bc
pha
lda z80_b
pha
jsr __PRINT_DIGIT    ;- call __PRINT_DIGIT
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
dec z80_b            ;- djnz __PRINTU_CONT
jne __PRINTU_CONT
rts                  ;- ret
                     ;- ENDP

__PRINT_MINUS: ; PRINT the MINUS (-) sign. CALLER must preserve registers
lda #'-'             ;- ld a,'-'
jmp __PRINT_DIGIT    ;- jp __PRINT_DIGIT
                     ;- __PRINT_DIGIT EQU __PRINTCHAR ; PRINTS the char in A register, and puts its attrs

