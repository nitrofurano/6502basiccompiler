__ABS32:
lda #$80       ;- bit 7,d
cmp z80_d
bne *+3        ;- ret z
rts

__NEG32: ; Negates DEHL (Two's complement)
lda z80_l      ;- ld a,l
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld l,a
sta z80_l
lda z80_h      ;- ld a,h
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld h,a
sta z80_h
lda z80_e      ;- ld a,e
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld e,a
sta z80_e
lda z80_d      ;- ld a,d
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld d,a
sta z80_d
inc z80_l      ;- inc l
beq *+3        ;- ret nz
rts
inc z80_h      ;- inc h
beq *+3        ;- ret nz
rts
inc z80_e      ;- inc de
bne *+4
inc z80_d
rts            ;- ret

