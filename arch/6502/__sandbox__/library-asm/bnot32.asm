; vim:ts=4:et:
; FASTCALL bitwise or 32 version.
; result in DE,HL
; __FASTCALL__ version (operands: A, H)
; Performs 32bit NEGATION (cpl)
; Input: DE,HL
; Output: DE,HL <- NOT DE,HL
;
__BNOT32:
lda z80_l      ;- ld a,l
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld l,a
sta z80_l
lda z80_h      ;- ld a,h
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld h,a
sta z80_h
lda z80_e      ;- ld a,e
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld e,a
sta z80_e
lda z80_d      ;- ld a,d
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld d,a
sta z80_d
rts            ;- ret

