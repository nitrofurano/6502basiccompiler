; Returns SGN (SIGN) for 8 bits signed integer

__SGNI8:
ora z80_a      ;- or a
bne *+3        ;- ret z
rts
lda #1         ;- ld a,1
sta z80_a
bmi *+3        ;- ret p
rts
eor #$ff       ;- neg
clc
adc #$01
rts            ;- ret

