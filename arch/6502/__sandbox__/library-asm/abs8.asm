; Returns absolute value for 8 bit signed integer
;
__ABS8:
eor z80_a    ;- or a
bmi *+3      ;- ret p
rts
eor #$ff     ;- neg
clc
adc #$01
rts          ;- ret

