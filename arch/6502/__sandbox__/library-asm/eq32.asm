__EQ32:	; Test if 32bit value HLDE equals top of the stack
; Returns result in A: 0 = False, FF = True
lda z80_c             ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                    ;- pop bc ; Return address
sta z80_b
pla
sta z80_c
lda z80_c              ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
ora z80_a           ;- or a	; Reset carry flag
pla                 ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_l           ;- sbc hl,bc ; Low part
sbc z80_c
sta z80_l
lda z80_h
sbc z80_b
sta z80_h
lda z80_e           ;- ex_de_hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla                 ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_l           ;- sbc hl,bc ; Hight part
sbc z80_c
sta z80_l
lda z80_h
sbc z80_b
sta z80_h
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_c            ;- push bc ; CALLEE
pha
lda z80_b
pha
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_h          ;- ld a,h
sta z80_a
ora z80_l          ;- or l
ora z80_d          ;- or d
ora z80_e          ;- or e   ; a = 0 only if HLDE = 0
sec                ;- sub 1  ; sets carry flag only if a = 0
sbc #1
sec                ;- sbc a,a
sbc z80_a
rts                ;- ret

