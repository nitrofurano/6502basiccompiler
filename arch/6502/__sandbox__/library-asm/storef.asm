__PISTOREF:	; Indect Stores a float (A, E, D, C, B) at location stored in memory, pointed by (IX + HL)
lda z80_e            ;- push de
pha
lda z80_d
pha
lda z80_e            ;- ex de,hl	; DE <- HL
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl		; HL <- IX
sta z80_h
pla
sta z80_l
clc                  ;- add hl,de  ; HL <- IX + HL
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
pla                  ;- pop de
sta z80_d
pla
sta z80_e

__ISTOREF:  ; Load address at hl, and stores A,E,D,C,B registers at that address. Modifies A' register
ldx z80_ap         ;- ex af,af'
sta z80_ap
txa
ldy #$00           ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l          ;- inc hl
bne *+4
inc z80_h
ldy #$00           ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_a          ;- ld l,a     ; HL = (HL)
sta z80_l
ldx z80_ap         ;- ex af,af'
sta z80_ap
txa

__STOREF:	; Stores the given FP number in A EDCB at address HL
lda z80_a      ;- ld (hl),a
ldy #$00
sta (z80_hl),y
inc z80_l      ;- inc hl
bne *+4
inc z80_h
lda z80_e      ;- ld (hl),e
ldy #$00
sta (z80_hl),y
inc z80_l      ;- inc hl
bne *+4
inc z80_h
lda z80_d      ;- ld (hl),d
ldy #$00
sta (z80_hl),y
inc z80_l      ;- inc hl
bne *+4
inc z80_h
lda z80_c      ;- ld (hl),c
ldy #$00
sta (z80_hl),y
inc z80_l      ;- inc hl
bne *+4
inc z80_h
lda z80_b      ;- ld (hl),b
ldy #$00
sta (z80_hl),y
rts            ;- ret

