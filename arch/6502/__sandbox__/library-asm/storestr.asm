; vim:ts=4:et:sw=4
; Stores value of current string pointed by DE register into address pointed by HL
; Returns DE = Address pointer  (&a$)
; Returns HL = HL               (b$ => might be needed later to free it from the heap)
;
; e.g. => HL = _variableName    (DIM _variableName$)
;         DE = Address into the HEAP
;
; This function will resize (REALLOC) the space pointed by HL
; before copying the content of b$ into a$
;
#include once <strcpy.asm>
;
__PISTORE_STR:          ; Indirect assignement at (IX + BC)
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_l            ;- add hl,bc
clc
adc z80_c
sta z80_l
lda z80_h
adc z80_b
sta z80_h

__ISTORE_STR:           ; Indirect assignement, hl point to a pointer to a pointer to the heap!
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_c            ;- ld l,c             ; HL = (HL)
sta z80_l

__STORE_STR:
lda z80_e            ;- push de             ; Pointer to b$
pha
lda z80_d
pha
lda z80_l            ;- push hl             ; Array pointer to variable memory address
pha
lda z80_h
pha
ldy #$00             ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_c            ;- ld l,c             ; HL = (HL)
sta z80_l
jsr __STRASSIGN      ;- call __STRASSIGN    ; HL (a$) = DE (b$); HL changed to a new dynamic memory allocation
lda z80_e            ;- ex de,hl           ; DE = new address of a$
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla                  ;- pop hl              ; Recover variable memory address pointer
sta z80_h
pla
sta z80_l
lda z80_e            ;- ld (hl),e
ldy #$00
sta (z80_hl),y
inc z80_l            ;- inc hl
bne *+4
inc z80_h
lda z80_d            ;- ld (hl),d          ; Stores a$ ptr into elemem ptr
ldy #$00
sta (z80_hl),y
pla                  ;- pop hl              ; Returns ptr to b$ in HL (Caller might needed to free it from memory)
sta z80_h
pla
sta z80_l
rts                  ;- ret

