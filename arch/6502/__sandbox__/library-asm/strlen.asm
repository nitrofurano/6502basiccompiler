; Returns len if a string
; If a string is NULL, its len is also 0
; Result returned in HL
;
__STRLEN:	; Direct FASTCALL entry
lda z80_h       ;- ld a,h
sta z80_a
ora z80_l       ;- or l
bne *+3         ;- ret z
rts
ldy #$00        ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l       ;- inc hl
bne *+4
inc z80_h
ldy #$00        ;- ld h,(hl)  ; LEN(str) in HL
lda (z80_hl),y
sta z80_h
lda z80_a       ;- ld l,a
sta z80_l
rts             ;- ret


