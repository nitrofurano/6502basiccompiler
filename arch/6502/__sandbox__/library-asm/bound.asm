; ---------------------------------------------------------
; Copyleft (k)2011 by Jose Rodriguez (a.k.a. Boriel)
; http://www.boriel.com
; ZX BASIC Compiler http://www.zxbasic.net
; This code is released under the BSD License
; ---------------------------------------------------------
; Implements bothe the LBOUND(array, N) and RBOUND(array, N) function
; Parameters:
;   HL = N (dimension)
;   [stack - 2] -> LBound table for the var
;   Returns entry [N] in HL
;
__BOUND:
asl z80_l            ;- add hl,hl      ; hl *= 2
rol z80_h
lda z80_e            ;- ex_de_hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
tsx                  ;- ex (sp),hl     ; __CALLEE
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
clc                  ;- add hl,de      ; hl += OFFSET __LBOUND._xxxx
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
ldy #$00             ;- ld e,(hl)      ; de = (hl)
lda (z80_hl),y
sta z80_e
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
lda z80_e            ;- ex de,hl       ; hl = de => returns result in HL
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
rts                  ;- ret

