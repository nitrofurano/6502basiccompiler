; Stores FP number in A ED CB at location HL+IX
; HL = Offset
; IX = Stack Frame
; A ED CB = FP Number
;
#include once <storef.asm>
;
; Stored a float number in A ED CB into the address pointed by IX + HL
__PSTOREF:
lda z80_e            ;- push de
pha
lda z80_d
pha
lda z80_e            ;- ex de,hl  ; DE <- HL
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl	   ; HL <- IX
sta z80_h
pla
sta z80_l
clc                  ;- add hl,de ; HL <- IX + DE
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
pla                  ;- pop de
sta z80_d
pla
sta z80_e
jmp __STOREF         ;- jp __STOREF

