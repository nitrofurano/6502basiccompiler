; This routine is called if --strict-boolean was set at the command line.
; It will make any boolean result to be always 0 or 1
;
__NORMALIZE_BOOLEAN:
ora z80_a      ;- or a
bne *+3        ;- ret z
rts
lda #1         ;- ld a,1
sta z80_a
rts            ;- ret

