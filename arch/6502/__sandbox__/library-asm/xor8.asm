; vim:ts=4:et:
; FASTCALL boolean xor 8 version.
; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: A, H)
; Performs 8bit xor 8bit and returns the boolean
;
__XOR16:
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
lda z80_a      ;- ld h,a
sta z80_h
lda z80_d      ;- ld a,d
sta z80_a
ora z80_e      ;- or e
__XOR8:
sec            ;- sub 1
sbc #1
sec            ;- sbc a,a
sbc z80_a
lda z80_a      ;- ld l,a    ;- l = 00h or FFh
sta z80_l
lda z80_h      ;- ld a,h
sta z80_a
sec            ;- sub 1
sbc #1
sec            ;- sbc a,a   ;- a = 00h or FFh
sbc z80_a
eor z80_l      ;- xor l
rts            ;- ret

