; Returns SGN (SIGN) for 32, 16 and 8 bits signed integers, Fixed and FLOAT
               ;- PROC
               ;- LOCAL __ENDSGN
__SGNF:
ora z80_b      ;- or b
ora z80_c      ;- or c
ora z80_d      ;- or d
ora z80_e      ;- or e
bne *+3        ;- ret z
rts
lda z80_e      ;- ld a,e
sta z80_a
jmp __ENDSGN   ;- jr __ENDSGN
__SGNF16:
__SGNI32:
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
ora z80_e      ;- or e
ora z80_d      ;- or d
bne *+3        ;- ret z
rts
lda z80_d      ;- ld a,d
sta z80_a
jmp __ENDSGN   ;- jr __ENDSGN
__SGNI16:
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
bne *+3        ;- ret z
rts
lda z80_h      ;- ld a,h
sta z80_a
__ENDSGN:
ora z80_a      ;- or a
lda #1         ;- ld a,1
sta z80_a
bmi *+3        ;- ret p
rts
eor #$ff       ;- neg
clc
adc #$01
rts            ;- ret
               ;- ENDP

