__SHL32: ; Left Logical Shift 32 bits
asl z80_l      ;- sla l
rol z80_h      ;- rl h
rol z80_e      ;- rl e
rol z80_d      ;- rl d
rts            ;- ret

