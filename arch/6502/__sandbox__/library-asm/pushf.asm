; Routine to push Float pointed by HL
; Into the stack. Notice that the hl points to the last
; byte of the FP number.
; Uses H'L' B'C' and D'E' to preserve ABCDEHL registers
;
__FP_PUSH_REV:
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
pla                  ;- pop bc ; Return Address
sta z80_b
pla
sta z80_c
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
lda z80_l            ;- dec_hl
bne dec_hl_no_dec_h
dec z80_h
dec_hl_no_dec_h:
dec z80_l
ldy #$00             ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
lda z80_l            ;- dec_hl
bne dec_hl_no_dec_h
dec z80_h
dec_hl_no_dec_h:
dec z80_l
lda z80_e            ;- push de
pha
lda z80_d
pha
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
lda z80_l            ;- dec_hl
bne dec_hl_no_dec_h
dec z80_h
dec_hl_no_dec_h:
dec z80_l
ldy #$00             ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
lda z80_l            ;- dec_hl
bne dec_hl_no_dec_h
dec z80_h
dec_hl_no_dec_h:
dec z80_l
lda z80_e            ;- push de
pha
lda z80_d
pha
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
lda z80_e            ;- push de
pha
lda z80_d
pha
lda z80_c            ;- push bc ; Return Address
pha
lda z80_b
pha
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
rts                  ;- ret


