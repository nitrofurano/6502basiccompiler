; -------------------------------------------------------------
; 32 bit logical NOT
; -------------------------------------------------------------
;
__NOT32:	; A = ¬A
lda z80_d      ;- ld a,d
sta z80_a
ora z80_e      ;- or e
ora z80_h      ;- or h
ora z80_l      ;- or l
sec            ;- sub 1	    ;- Gives CARRY only if 0
sbc #1
sec            ;- sbc a,a   ;- Gives 0 if not carry, FF otherwise
sbc z80_a
rts            ;- ret


