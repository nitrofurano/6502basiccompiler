; vim:ts=4:et:sw=4
; Stores an string (pointer to the HEAP by DE) into the address pointed
; by (IX + BC). No new copy of the string is created into the HEAP, since
; it's supposed it's already created (temporary string)
;
#include once <storestr2.asm>
;
__PSTORE_STR2:
lda z80_ix           ;- push ix
pha
lda z80_ix+1
pha
pla                  ;- pop hl
sta z80_h
pla
sta z80_l
lda z80_l            ;- add hl,bc
clc
adc z80_c
sta z80_l
lda z80_h
adc z80_b
sta z80_h
jmp __STORE_STR2      ;- jp __STORE_STR2

