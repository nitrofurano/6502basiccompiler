#include once <neg32.asm>
#include once <u32tofreg.asm>
;
__F16TOFREG:	; Converts a 16.16 signed fixed point (stored in DEHL)
; to a Floating Point Number returned in (C ED CB)
                     ;- PROC
                     ;- LOCAL __F16TOFREG2
lda z80_d            ;- ld a,d
sta z80_a
ora z80_a            ;- or a		; Test sign
jpl __F16TOFREG2     ;- jp p,__F16TOFREG2	; It was positive, proceed as 32bit unsigned
jsr __NEG32          ;- call __NEG32		; Convert it to positive
jsr __F16TOFREG2     ;- call __F16TOFREG2	; Convert it to Floating point
lda z80_e            ;- set 7,e			; Put the sign bit (negative) in the 31bit of mantissa
ora #$80
sta z80_e
rts                  ;- ret

__F16TOFREG2:	; Converts an unsigned 32 bit integer (DEHL)
; to a Floating point number returned in C DE HL
lda z80_d            ;- ld a,d
sta z80_a
ora z80_e            ;- or e
ora z80_h            ;- or h
ora z80_l            ;- or l
lda z80_h            ;- ld b,h
sta z80_b
lda z80_l            ;- ld c,l
sta z80_c
bne *+3              ;- ret z       ; Return 00 0000 0000 if 0
rts
lda z80_e            ;- push de
pha
lda z80_d
pha
lda z80_l            ;- push hl
pha
lda z80_h
pha
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                  ;- pop de  ; Loads integer into B'C' D'E'
sta z80_d
pla
sta z80_e
pla                  ;- pop bc
sta z80_b
pla
sta z80_c
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda #112             ;- ld l,112	; Exponent
sta z80_l
lda #<0
sta z80_c
lda #>0
sta z80_b
lda z80_b            ;- ld d,b
sta z80_d
lda z80_c            ;- ld e,c
sta z80_e
jmp __U32TOFREG_LOOP ;- jp __U32TOFREG_LOOP ; Proceed as an integer
                     ;- ENDP

