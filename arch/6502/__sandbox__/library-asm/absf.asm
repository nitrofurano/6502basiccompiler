; ABS value for a floating point A EDCB
__ABSF:
lda z80_e    ;- res 7, e	; Sets sign to positive. Thats all! ;-)
and #$7F
sta z80_e
rts          ;- ret
