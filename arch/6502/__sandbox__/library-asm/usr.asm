; Emulates the USR Sinclair BASIC function
; Result value returns in BC
; We use HL for returning values, su we must
; copy BC into HL before returning
; The incoming parameter is HL (Address to JUMP)
;
#include once <table_jump.asm>
;
USR:
jsr CALL_HL          ;- call CALL_HL
lda z80_b            ;- ld h,b
sta z80_h
lda z80_c            ;- ld l,c
sta z80_l
rts                  ;- ret

