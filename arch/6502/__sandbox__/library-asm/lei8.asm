#include once <lti8.asm>
;
__LEI8: ; Signed <= comparison for 8bit int
; A <= H (registers)
sec            ;- sub h
sbc z80_h
jne __LTI      ;- jp nz, __LTI    ;- not 0, proceed as A < H
sec            ;- dec a           ;- Sets A to True
sbc #$01
rts            ;- ret
