__OR32:  ; Performs logical operation A AND B
		 ; between DEHL and TOP of the stack.
		 ; Returns A = 0 (False) or A = FF (True)
;
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
ora z80_d      ;- or d
ora z80_e      ;- or e
pla            ;- pop hl ; Return address
sta z80_h
pla
sta z80_l
pla            ;- pop de
sta z80_d
pla
sta z80_e
ora z80_d      ;- or d
ora z80_e      ;- or e
pla            ;- pop de
sta z80_d
pla
sta z80_e
ora z80_d      ;- or d
ora z80_e      ;- or e   ; A = 0 only if DEHL and TOP of the stack = 0
jmp (z80_hl)   ;- jp (hl) ; Faster "Ret"


