#include once <lti8.asm>
;
__LTI16: ; Test 8 bit values HL < DE
; Returns result in A: 0 = False, !0 = True
eor z80_a      ;- xor a
lda z80_l      ;- sbc hl,de
sbc z80_e
sta z80_l
lda z80_h
sbc z80_d
sta z80_h
jmp __LTI2     ;- jp __LTI2

