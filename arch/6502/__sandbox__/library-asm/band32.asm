; FASTCALL bitwise and 32 version.
; Performs 32bit and 32bit and returns the bitwise
; result in DE,HL
; First operand in DE,HL 2nd operand into the stack
;
__BAND32:
lda z80_h      ;- ld b,h
sta z80_b
lda z80_l      ;- ld c,l       ; BC <- HL
sta z80_c
pla            ;- pop hl  ; Return address
sta z80_h
pla
sta z80_l
tsx            ;- ex (sp),hl   ; HL <- Lower part of 2nd Operand
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
lda z80_b      ;- ld a,b
sta z80_a
and z80_h      ;- and h
lda z80_a      ;- ld b,a
sta z80_b
lda z80_c      ;- ld a,c
sta z80_a
and z80_l      ;- and l
lda z80_a      ;- ld c,a     ;- BC <- BC & HL
sta z80_c
pla            ;- pop hl  ; Return address
sta z80_h
pla
sta z80_l
tsx            ;- ex (sp),hl ; HL <- High part of 2nd Operand
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
lda z80_d      ;- ld a,d
sta z80_a
and z80_h      ;- and h
lda z80_a      ;- ld d,a
sta z80_d
lda z80_e      ;- ld a,e
sta z80_a
and z80_l      ;- and l
lda z80_a      ;- ld e,a ; DE <- DE & HL
sta z80_e
lda z80_b      ;- ld h,b
sta z80_h
lda z80_c      ;- ld l,c ; HL <- BC  ; Always return DE,HL pair regs
sta z80_l
rts            ;- ret

