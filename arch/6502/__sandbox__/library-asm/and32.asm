; FASTCALL boolean and 32 version.
; Performs 32bit and 32bit and returns the boolean
; result in Accumulator (0 False, not 0 True)
; First operand in DE,HL 2nd operand into the stack

__AND32:
lda z80_l    ;- ld a, l
sta z80_a
ora z80_h    ;- or h
ora z80_e    ;- or e
ora z80_d    ;- or d
sec          ;- sub 1
sbc #1
sbc z80_a    ;- sbc a
lda z80_a    ;- ld c, a
sta z80_c
pla          ;- pop hl
sta z80_h
pla
sta z80_l
pla          ;- pop de
sta z80_d
pla
sta z80_e
lda z80_d    ;- ld a, d
lda z80_a
ora z80_e    ;- or e
pla          ;- pop de
sta z80_d
pla
sta z80_e
ora z80_d    ;- or d
ora z80_e    ;- or e
sec          ;- sub 1
sbc #1
sbc z80_a    ;- sbc a
ora z80_c    ;- or c
eor #$ff     ;- cpl
jmp (z80_hl) ;- jp (hl)


