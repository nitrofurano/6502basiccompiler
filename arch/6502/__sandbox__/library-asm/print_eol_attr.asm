; Calls PRINT_EOL and then COPY_ATTR, so saves
; 3 bytes
;
#include once <print.asm>
#include once <copy_attr.asm>
;
PRINT_EOL_ATTR:
jsr PRINT_EOL        ;- call PRINT_EOL
jmp COPY_ATTR        ;- jp COPY_ATTR
