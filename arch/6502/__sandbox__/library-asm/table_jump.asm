JUMP_HL_PLUS_2A: ; Does JP (HL + A*2) Modifies DE. Modifies A
clc                  ;- add a,a
adc z80_a

JUMP_HL_PLUS_A:  ; Does JP (HL + A) Modifies DE
lda z80_a            ;- ld e,a
sta z80_e
lda #0               ;- ld d,0
sta z80_d

JUMP_HL_PLUS_DE: ; Does JP (HL + DE)
clc                  ;- add hl,de
lda z80_l
adc z80_e
sta z80_l
lda z80_h
adc z80_d
sta z80_h
ldy #$00             ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
CALL_HL:
jmp (z80_hl)         ;- jp (hl)

