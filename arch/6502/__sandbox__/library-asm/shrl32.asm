__SHRL32: ; Right Logical Shift 32 bits
lsr z80_d      ;- srl d
ror z80_e      ;- rr e
ror z80_h      ;- rr h
ror z80_l      ;- rr l
rts            ;- ret

