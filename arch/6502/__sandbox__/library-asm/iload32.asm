; __FASTCALL__ routine which
; loads a 32 bits integer into DE,HL
; stored at position pointed by POINTER HL
; DE,HL <-- (HL)
;
__ILOAD32:
ldy #$00             ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l            ;- inc hl
bne *+4
inc z80_h
ldy #$00             ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_a            ;- ld l,a
sta z80_l
lda z80_e            ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
rts                  ;- ret

