; FASTCALL boolean and 16 version.
; result in Accumulator (0 False, not 0 True)
; __FASTCALL__ version (operands: DE, HL)
; Performs 16bit and 16bit and returns the boolean
;
__AND16:
lda z80_h    ;- ld a, h
sta z80_a
ora z80_l    ;- or l
bne *+3      ;- ret z
rts
lda z80_d    ;- ld a, d
ora z80_e    ;- or e
rts          ;- ret

