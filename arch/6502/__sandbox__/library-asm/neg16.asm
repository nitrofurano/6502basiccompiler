; Negates HL value (16 bit)
__ABS16:
lda #$80       ;- bit 7,h
cmp z80_h
bne *+3        ;- ret z
rts

__NEGHL:
lda z80_l      ;- ld a,l			; HL = -HL
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld l,a
sta z80_l
lda z80_h      ;- ld a,h
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld h,a
sta z80_h
inc z80_l      ;- inc hl
bne *+4
inc z80_h
rts            ;- ret

