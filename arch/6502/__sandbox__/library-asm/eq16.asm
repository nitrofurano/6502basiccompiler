__EQ16:	; Test if 16bit values HL == DE
; Returns result in A: 0 = False, FF = True
ora z80_a      ;- or a	   ;- Reset carry flag
lda z80_l      ;- sbc hl,de
sbc z80_e
sta z80_l
lda z80_h
sbc z80_d
sta z80_h
lda z80_h      ;- ld a,h
sta z80_a
ora z80_l      ;- or l
sec            ;- sub 1    ;- sets carry flag only if a = 0
sbc #1
lda #$00       ;- sbc a,a
sec
rts            ;- ret

