#include once <_mul32.asm>
;
__MUL32:	; multiplies 32 bit un/signed integer.
; First operand stored in DEHL, and 2nd onto stack
; Lowest part of 2nd operand on top of the stack
; returns the result in DE.HL
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                   ;- pop hl	; Return ADDRESS
sta z80_h
pla
sta z80_l
pla                   ;- pop de	; Low part
sta z80_d
pla
sta z80_e
tsx                   ;- ex (sp),hl ; CALLEE -> HL = High part
lda $0103,x
ldy z80_h
sta z80_h
tya
sta $0103,x
lda $0104,x
ldy z80_l
sta z80_l
tya
sta $104,x
lda z80_e             ;- ex de,hl
ldx z80_l
stx z80_e
sta z80_l
lda z80_d
ldx z80_h
stx z80_d
sta z80_h
jsr __MUL32_64START   ;- call __MUL32_64START
__TO32BIT:            ; Converts H'L'HLB'C'AC to DEHL (Discards H'L'HL)
lda z80_c            ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_c             ;- push bc
pha
lda z80_b
pha
lda z80_c             ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla                   ;- pop de
sta z80_d
pla
sta z80_e
lda z80_a             ;- ld h,a
sta z80_h
lda z80_c             ;- ld l,c
sta z80_l
rts                   ;- ret

