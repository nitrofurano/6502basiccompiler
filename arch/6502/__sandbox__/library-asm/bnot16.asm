; vim:ts=4:et:
; FASTCALL bitwise or 16 version.
; result in HL
; __FASTCALL__ version (operands: A, H)
; Performs 16bit NEGATION
; Input: HL
; Output: HL <- NOT HL
;
__BNOT16:
lda z80_h      ;- ld a,h
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld h,a
sta z80_h
lda z80_l      ;- ld a,l
sta z80_a
eor #$ff       ;- cpl
lda z80_a      ;- ld l,a
sta z80_l
rts            ;- ret

