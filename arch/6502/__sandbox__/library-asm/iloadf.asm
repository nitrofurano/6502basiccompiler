; __FASTCALL__ routine which
; loads a 40 bits floating point into A ED CB
; stored at position pointed by POINTER HL
;A DE, BC <-- ((HL))
;
__ILOADF:
ldy #$00        ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l       ;- inc hl
bne *+4
inc z80_h
ldy #$00        ;- ld h,(hl)
lda (z80_hl),y
sta z80_h
lda z80_a       ;- ld l,a
sta z80_l
;
; __FASTCALL__ routine which
; loads a 40 bits floating point into A ED CB
; stored at position pointed by POINTER HL
;A DE, BC <-- (HL)
;
__LOADF:    ; Loads a 40 bits FP number from address pointed by HL
ldy #$00        ;- ld a,(hl)
lda (z80_hl),y
sta z80_a
inc z80_l       ;- inc hl
bne *+4
inc z80_h
ldy #$00        ;- ld e,(hl)
lda (z80_hl),y
sta z80_e
inc z80_l       ;- inc hl
bne *+4
inc z80_h
ldy #$00        ;- ld d,(hl)
lda (z80_hl),y
sta z80_d
inc z80_l       ;- inc hl
bne *+4
inc z80_h
ldy #$00        ;- ld c,(hl)
lda (z80_hl),y
sta z80_c
inc z80_l       ;- inc hl
bne *+4
inc z80_h
ldy #$00        ;- ld b,(hl)
lda (z80_hl),y
sta z80_b
rts             ;- ret

