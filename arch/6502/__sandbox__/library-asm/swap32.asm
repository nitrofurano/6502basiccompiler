; Exchanges current DE HL with the
; ones in the stack
;
__SWAP32:
pla           ;- pop bc ; Return address
sta z80_b
pla
sta z80_c
lda z80_c     ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla            ;- pop hl	; exx'
sta z80_h
pla
sta z80_l
pla            ;- pop de
sta z80_d
pla
sta z80_e
lda z80_c     ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_e      ;- push de ; exx
pha
lda z80_d
pha
lda z80_l      ;- push hl
pha
lda z80_h
pha
lda z80_c      ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
lda z80_e      ;- push de
pha
lda z80_d
pha
lda z80_l      ;- push hl
pha
lda z80_h
pha
lda z80_c      ;- exx
ldx z80_cp
stx z80_c
sta z80_cp
lda z80_b
ldx z80_bp
stx z80_b
sta z80_bp
lda z80_e
ldx z80_ep
stx z80_e
sta z80_ep
lda z80_d
ldx z80_dp
stx z80_d
sta z80_dp
lda z80_l
ldx z80_lp
stx z80_l
sta z80_lp
lda z80_h
ldx z80_hp
stx z80_h
sta z80_hp
pla            ;- pop hl
sta z80_h
pla
sta z80_l
pla            ;- pop de
sta z80_d
pla
sta z80_e
lda z80_c      ;- push bc
pha
lda z80_b
pha
rts            ;- ret

