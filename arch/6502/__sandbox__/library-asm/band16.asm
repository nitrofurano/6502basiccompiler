; vim:ts=4:et:
; FASTCALL bitwise and16 version.
; result in hl
; __FASTCALL__ version (operands: A, H)
; Performs 16bit or 16bit and returns the boolean
; Input: HL, DE
; Output: HL <- HL AND DE
;
__BAND16:
lda z80_h    ;- ld a,h
sta z80_a
and z80_d    ;- and d
lda z80_a    ;- ld h,a
sta z80_h
lda z80_l    ;- ld a,l
sta z80_a
and z80_e    ;- and e
lda z80_a    ;- ld l,a
sta z80_l
rts          ;- ret

