lda z80_e    ;- get z80 de (address) register value from page 0, and write to vdp register
sta $2006
lda z80_d
sta $2006

  ldy #$00
  ldx z80_b
  beq ldirvm_last_page
ldirvm_loop:
  lda (z80_hl),y

  ;sta (z80_de),y
  sta $2007

  iny
  bne ldirvm_loop
  inc z80_h
  inc z80_d
  dex
  bne ldirvm_loop
ldirvm_last_page:
  lda z80_c
  beq ldirvm_end
ldirvm_last_page_loop:
  lda (z80_hl),y

  ;sta (z80_de),y
  sta $2007

  iny
  cpy z80_c
  bne ldirvm_last_page_loop
ldirvm_end:
  stx z80_c
  stx z80_b
  tya
  clc
  adc z80_l
  sta z80_l
  bcc *+4
  inc z80_h
  tya
  clc
  adc z80_e
  sta z80_e
  bcc *+4
  inc z80_d
  rts

