lda z80_e    ;- get z80 de (address) register value from page 0, and write to vdp register
sta $2006
lda z80_d
sta $2006

  ldy #$00
  ldx z80_b
  beq filvrm_last_page
filvrm_loop:
  lda z80_a

  ;sta (z80_de),y
  sta $2007

  iny
  bne filvrm_loop
  inc z80_h
  inc z80_d
  dex
  bne filvrm_loop
filvrm_last_page:
  lda z80_c
  beq filvrm_end
filvrm_last_page_loop:
  lda z80_a

  ;sta (z80_de),y
  sta $2007

  iny
  cpy z80_c
  bne filvrm_last_page_loop
filvrm_end:
  stx z80_c
  stx z80_b
  tya
  clc
  adc z80_l
  sta z80_l
  bcc *+4
  inc z80_h
  tya
  clc
  adc z80_e
  sta z80_e
  bcc *+4
  inc z80_d
  rts

