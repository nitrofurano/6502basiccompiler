
lda z80_e    ;- get z80 de (address) register value from page 0, and write to vdp register
sta $2006
lda z80_d
sta $2006
lda $2007
sta z80_a   ;- get z80 a (value) register value from page 0, and write to vdp register
rts

