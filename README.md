# Boriel’s ZX-Basic Compiler - 6502 arch
For now, this is only a kind of "stone soup", since it seems there are no ansi-basic cross-compilers for 6502-based hardware.

The main reference is Boriel’s ZX-Basic Compiler ( http://boriel.com/wiki/en/index.php/ZXBasic ), since perhaps Z80 and 6502 can be coded exactly in the same way (even considering there are no ix/iy registers, 16-bit registers, 16-bit aritmethic and boolean operands, ldir and alike, etc.).

The documentation and converter posted in an atariage forum (https://atariage.com/forums/topic/247596-z80-6502-recompiler-used-with-pentagram-port/ ) also helps a lot in this process

The fact is, compared with Z80, i have almost no idea how 6502 works, and this project is mostly for collecting information, documentation and experiences.

If someone reading this text can substantially help it improving, be welcome!

(original Boriel’s ZX-Basic Compiler git url: https://bitbucket.org/zxbasic/zxbasic )


